# 0.2.3 - 2023/07/20

### Fixed
- API errors

# 0.2.2 - 2023/05/24

### Fixed
- Wrong API endpoint for quotes

# 0.2.1 - 2023/05/24

### Fixed
- Quote API

# 0.2.0 - 2023/05/10

### Changed
- Improved performances
- Various UI improvements

### Fixed
- Quote API

# 0.1.5 - 2021/08/17

### Added
- Splashscreen
- Spanish translation (thanks to @sguinetti)

### Fixed
- Search (again)

# 0.1.4 - 2021/07/28

### Fixed
- Search

# 0.1.3 - 2021/07/21

### Fixed
- Some API errors

### Changed
- Various UI elements

# 0.1.2 - 2021/07/12

### Fixed
- Charts animation

# 0.1.1 - 2021/07/09

### Removed
- Splashscreen

### Fixed
- Installation on older devices

# 0.1.0 - 2021/07/08

### Added
- Overview
- Watchlist
- Search feature
- Light/Dark theme
