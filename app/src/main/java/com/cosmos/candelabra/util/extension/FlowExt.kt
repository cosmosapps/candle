package com.cosmos.candelabra.util.extension

import kotlinx.coroutines.flow.SharedFlow

val <T> SharedFlow<T>.latest: T? get() = replayCache.lastOrNull()
