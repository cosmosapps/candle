package com.cosmos.candelabra.ui.watchlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cosmos.candelabra.data.model.Chart
import com.cosmos.candelabra.data.model.ChartPeriod
import com.cosmos.candelabra.data.model.ChartWithQuote
import com.cosmos.candelabra.data.model.Resource
import com.cosmos.candelabra.data.model.db.Quote
import com.cosmos.candelabra.data.repository.StockRepository
import com.cosmos.candelabra.data.repository.YahooFinanceRepository
import com.cosmos.candelabra.di.DispatchersModule.DefaultDispatcher
import com.cosmos.candelabra.util.extension.latest
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WatchlistViewModel @Inject constructor(
    stockRepository: StockRepository,
    private val yahooFinanceRepository: YahooFinanceRepository,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : ViewModel() {

    private var fetchJob: Job? = null

    private var symbols: List<String> = emptyList()

    private val _userQuotes: Flow<List<Quote>> = stockRepository
        .getAllQuotes()
        .onEach { quotes -> symbols = quotes.map { it.symbol } }
        .onEach { fetchChartsInRealTime(symbols) }
        .onCompletion { stopFetching() }
        .distinctUntilChangedBy { it.size } // Send updates only when a quote is added or removed
        .flowOn(defaultDispatcher)

    private val _charts: MutableStateFlow<Resource<List<ChartWithQuote>>> =
        MutableStateFlow(Resource.Loading())
    val charts: StateFlow<Resource<List<ChartWithQuote>>>
        get() = _charts.asStateFlow()

    val userCharts: SharedFlow<List<ChartWithQuote>> = _userQuotes
        .map { quotes ->
            quotes.map { quote ->
                ChartWithQuote(
                    Chart(
                        quote.symbol,
                        quote.currency,
                        quote.price,
                        quote.previousClose,
                        emptyList()
                    ),
                    quote
                )
            }
        }
        .flowOn(defaultDispatcher)
        .shareIn(viewModelScope, SharingStarted.WhileSubscribed(5000), 1)

    private var currentCharts: List<String>? = null
    private var currentPeriod: ChartPeriod? = null
    private var currentInterval: Long? = null

    fun fetchChartsInRealTime(
        charts: List<String>? = currentCharts,
        chartPeriod: ChartPeriod = DEFAULT_PERIOD,
        interval: Long = DEFAULT_INTERVAL,
        forceRefresh: Boolean = false
    ) {
        val isSameQuery = (currentCharts == charts &&
                currentPeriod == chartPeriod &&
                currentInterval == interval)

        if (fetchJob?.isActive == true && isSameQuery && !forceRefresh) return

        fetchJob?.cancel()

        val immediate = forceRefresh || !isSameQuery

        charts?.let {
            currentCharts = it
            currentPeriod = chartPeriod
            currentInterval = interval

            fetchJob = viewModelScope.launch {
                yahooFinanceRepository
                    .getChartsInRealTime(it, chartPeriod, interval, immediate)
                    .onStart {
                        if (_charts.value !is Resource.Success ||
                            _charts.value.data.isNullOrEmpty()) {
                            _charts.value = Resource.Success(userCharts.latest ?: emptyList())
                        }
                    }
                    .collect { chartList ->
                        _charts.value = chartList
                    }
            }
        } ?: run {
            _charts.value = Resource.Error()
        }
    }

    private fun stopFetching() {
        fetchJob?.cancel()
    }

    companion object {
        private const val DEFAULT_INTERVAL: Long = 15 * 1000
        private val DEFAULT_PERIOD = ChartPeriod.DAY_1
    }
}
