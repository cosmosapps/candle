package com.cosmos.candelabra.ui.overview

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cosmos.candelabra.data.model.Resource
import com.cosmos.candelabra.data.model.db.Quote
import com.cosmos.candelabra.data.repository.StockRepository
import com.cosmos.candelabra.data.repository.YahooFinanceRepository
import com.cosmos.candelabra.di.DispatchersModule.DefaultDispatcher
import com.cosmos.candelabra.util.extension.latest
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OverviewViewModel @Inject constructor(
    stockRepository: StockRepository,
    private val yahooFinanceRepository: YahooFinanceRepository,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : ViewModel() {

    private var fetchJob: Job? = null

    private var symbols: List<String> = emptyList()

    val userQuotes: SharedFlow<List<Quote>> = stockRepository
        .getAllQuotes()
        .onEach { quotes -> symbols = quotes.map { it.symbol } }
        .onEach { fetchQuotesInRealTime(symbols) }
        .onCompletion { stopFetching() }
        .flowOn(defaultDispatcher)
        .shareIn(viewModelScope, SharingStarted.WhileSubscribed(5000), 1)

    private val _quotes: MutableStateFlow<Resource<List<Quote>>> =
        MutableStateFlow(Resource.Loading())
    val quotes: StateFlow<Resource<List<Quote>>>
        get() =  _quotes.asStateFlow()

    private var currentQuotes: List<String>? = null
    private var currentInterval: Long? = null

    fun fetchQuotesInRealTime(
        quotes: List<String>? = currentQuotes,
        interval: Long = DEFAULT_INTERVAL,
        forceRefresh: Boolean = false
    ) {
        val isSameQuery = (currentQuotes == quotes && currentInterval == interval)

        if (fetchJob?.isActive == true && isSameQuery && !forceRefresh) return

        fetchJob?.cancel()

        val immediate = forceRefresh || !isSameQuery

        quotes?.let {
            currentQuotes = it
            currentInterval = interval

            fetchJob = viewModelScope.launch {
                yahooFinanceRepository
                    .getQuotesInRealTime(it, interval, immediate)
                    .onStart {
                        if (_quotes.value !is Resource.Success ||
                            _quotes.value.data.isNullOrEmpty()) {
                            _quotes.value = Resource.Success(userQuotes.latest ?: emptyList())
                        }
                    }
                    .collect { quoteList ->
                        _quotes.value = quoteList
                    }
            }
        } ?: run {
            _quotes.value = Resource.Error()
        }
    }

    private fun stopFetching() {
        fetchJob?.cancel()
    }

    companion object {
        private const val DEFAULT_INTERVAL: Long = 10 * 1000
    }
}
