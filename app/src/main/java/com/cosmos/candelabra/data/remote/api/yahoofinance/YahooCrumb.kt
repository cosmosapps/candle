package com.cosmos.candelabra.data.remote.api.yahoofinance

import com.cosmos.candelabra.data.repository.PreferencesRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class YahooCrumb @Inject constructor(
    private val yahooFinanceCrumb: YahooFinanceCrumb,
    private val preferencesRepository: PreferencesRepository,
) {

    var crumb: String? = null

    suspend fun fetchCrumb(): String? {
        if (crumb != null) return crumb

        runCatching {
            yahooFinanceCrumb.getCrumb()
        }.onSuccess {
            crumb = it
            preferencesRepository.setCrumb(it)
        }

        return crumb
    }
}
