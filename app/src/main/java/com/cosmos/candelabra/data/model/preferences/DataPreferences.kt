package com.cosmos.candelabra.data.model.preferences

import androidx.datastore.preferences.core.longPreferencesKey

data class DataPreferences(
    val quotesLastRefresh: Long,
    val chartLastRefresh: Long,
    val chartsLastRefresh: Long
) {
    object PreferenceKeys {
        val QUOTES_LAST_REFRESH = longPreferencesKey("quotes_last_refresh")
        val CHART_LAST_REFRESH = longPreferencesKey("chart_last_refresh")
        val CHARTS_LAST_REFRESH = longPreferencesKey("charts_last_refresh")
    }
}
