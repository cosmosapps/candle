package com.cosmos.candelabra.data.local.mapper

import com.cosmos.candelabra.data.model.QuoteType
import com.cosmos.candelabra.data.model.SearchItem
import com.cosmos.candelabra.data.remote.api.yahoofinance.model.Search
import com.cosmos.candelabra.data.remote.api.yahoofinance.model.SearchQuote
import com.cosmos.candelabra.di.DispatchersModule.DefaultDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchMapper @Inject constructor(
    @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
) : Mapper<SearchQuote, SearchItem.Quote>(defaultDispatcher) {

    suspend fun dataToEntities(data: Search): List<SearchItem> {
        return data.quotes.map { toEntity(it) }
    }

    override suspend fun toEntity(from: SearchQuote): SearchItem.Quote {
        return with(from) {
            SearchItem.Quote(
                symbol,
                shortname ?: longname ?: "",
                exchange,
                QuoteType.fromString(quoteType)
            )
        }
    }
}
