package com.cosmos.candelabra.data.remote.api.yahoofinance

import com.cosmos.candelabra.data.repository.PreferencesRepository
import com.cosmos.candelabra.di.DispatchersModule.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.withContext
import okhttp3.FormBody
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class YahooInitializer @Inject constructor(
    private val yahooFinance: YahooFinance,
    private val yahooConsent: YahooConsent,
    private val yahooCrumb: YahooCrumb,
    private val yahooCookieJar: YahooCookieJar,
    private val preferencesRepository: PreferencesRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) {

    val isInitialized: Boolean
        get() = yahooCrumb.crumb != null

    suspend fun init(): Result<String?> {
        return when (shouldRefresh()) {
            true -> {
                Timber.d("Init from Network")
                initFromNetwork()
            }
            false -> {
                Timber.d("Init from Preferences")
                Result.success(yahooCrumb.crumb)
            }
        }
    }

    private suspend fun initFromNetwork(): Result<String?> {
        cleanCache()

        return runCatching {
            val body = yahooFinance.load()

            collectConsent(body)

            yahooCrumb.fetchCrumb()
        }.onFailure { throwable ->
            Timber.e(throwable, "Error while initializing Yahoo Finance from network")
        }
    }

    private suspend fun collectConsent(body: ResponseBody) = withContext(ioDispatcher) {
        val doc = Jsoup.parse(body.string())
        val csrfToken = doc.selectFirst("input[name=csrfToken]")?.attr("value")

        csrfToken?.let { token ->
            Timber.d("Consent has to be collected")

            val sessionId = doc.selectFirst("input[name=sessionId]")?.attr("value")
            val originalDoneUrl = doc.selectFirst("input[name=originalDoneUrl]")?.attr("value")
            val namespace = doc.selectFirst("input[name=namespace]")?.attr("value")

            val requestBody = FormBody.Builder()
                .add("csrfToken", token)
                .add("sessionId", sessionId.orEmpty())
                .addEncoded("originalDoneUrl", originalDoneUrl.orEmpty())
                .add("namespace", namespace.orEmpty())
                .add("reject", "reject")
                .build()

            yahooConsent.collectConsent(requestBody, sessionId.orEmpty())
        }
    }

    private suspend fun shouldRefresh(): Boolean {
        val cookiePreferences = preferencesRepository.getCookiePreferences().firstOrNull()
            ?: return true

        yahooCookieJar.initCookies()
        yahooCrumb.crumb = cookiePreferences.crumb

        val crumbNullEmpty = cookiePreferences.crumb.isNullOrEmpty()

        Timber.d("Crumb is null or empty: %s", crumbNullEmpty)
        Timber.d("Created: %s", cookiePreferences.created)
        Timber.d("Cookie jar size: %d", yahooCookieJar.size)

        return yahooCookieJar.isEmpty || // No cookie in the jar
                crumbNullEmpty || // No saved crumb
                cookiePreferences.created == null || // No saved date
                hasCrumbExpired(cookiePreferences.created) // Expired crumb
    }

    private fun hasCrumbExpired(created: Long): Boolean {
        return (System.currentTimeMillis() - created) >= EXPIRY
    }

    private suspend fun cleanCache() {
        preferencesRepository.removeCrumb()
        yahooCrumb.crumb = null
        yahooCookieJar.emptyJar()
    }

    companion object {
        private val EXPIRY = TimeUnit.HOURS.toMillis(24)
    }
}
