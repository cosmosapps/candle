package com.cosmos.candelabra.data.remote.api.yahoofinance

import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface YahooConsent {

    @POST("/v2/collectConsent")
    suspend fun collectConsent(@Body body: RequestBody, @Query("sessionId") sessionId: String)

    companion object {
        const val BASE_URL = "https://consent.yahoo.com/"
    }
}
