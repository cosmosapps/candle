package com.cosmos.candelabra.data.remote.api.yahoofinance

import retrofit2.http.GET

interface YahooFinanceCrumb {

    @GET("/v1/test/getcrumb")
    suspend fun getCrumb(): String

    companion object {
        const val BASE_URL = "https://query1.finance.yahoo.com/"
    }
}
