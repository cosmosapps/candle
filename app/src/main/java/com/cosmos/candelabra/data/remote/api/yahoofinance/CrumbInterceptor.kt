package com.cosmos.candelabra.data.remote.api.yahoofinance

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CrumbInterceptor @Inject constructor(private val yahooCrumb: YahooCrumb) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val url = request.url

        val newUrl = url.newBuilder()
            .addQueryParameter("crumb", yahooCrumb.crumb.orEmpty())
            .build()

        val newRequest = request.newBuilder()
            .url(newUrl)
            .build()

        return chain.proceed(newRequest)
    }
}
