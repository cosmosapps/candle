package com.cosmos.candelabra.data.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cookie")
data class Cookie(
    @PrimaryKey
    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "value") 
    val value: String,

    @ColumnInfo(name = "expires_at")
    val expiresAt: Long,

    @ColumnInfo(name = "domain")
    val domain: String,

    @ColumnInfo(name = "path") 
    val path: String,

    @ColumnInfo(name = "secure") 
    val secure: Boolean,

    @ColumnInfo(name = "http_only")
    val httpOnly: Boolean,

    @ColumnInfo(name = "persistent") 
    val persistent: Boolean,

    @ColumnInfo(name = "host_only")
    val hostOnly: Boolean
)
