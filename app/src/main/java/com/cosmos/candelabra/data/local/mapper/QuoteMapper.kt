package com.cosmos.candelabra.data.local.mapper

import com.cosmos.candelabra.data.model.db.Quote
import com.cosmos.candelabra.data.remote.api.yahoofinance.model.QuoteResult
import com.cosmos.candelabra.di.DispatchersModule.DefaultDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QuoteMapper @Inject constructor(
    @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
) : Mapper<QuoteResult, Quote>(defaultDispatcher) {

    override suspend fun toEntity(from: QuoteResult): Quote {
        return with(from) {
            Quote(
                symbol,
                shortName,
                currency,
                regularMarketPrice,
                regularMarketPreviousClose,
                regularMarketChange,
                regularMarketChangePercent,
                regularMarketOpen,
                regularMarketDayLow,
                regularMarketDayHigh,
                fiftyTwoWeekLow,
                fiftyTwoWeekHigh,
                regularMarketVolume,
                marketCap,
                trailingPE,
                earningsTimestamp?.times(1000),
                trailingAnnualDividendRate,
                dividendDate?.times(1000)
            )
        }
    }
}
