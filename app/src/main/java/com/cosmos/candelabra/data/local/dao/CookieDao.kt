package com.cosmos.candelabra.data.local.dao

import androidx.room.Dao
import androidx.room.Query
import com.cosmos.candelabra.data.model.db.Cookie

@Dao
abstract class CookieDao : BaseDao<Cookie> {

    @Query("SELECT * FROM cookie")
    abstract suspend fun getAllCookies(): List<Cookie>

    @Query("DELETE FROM cookie")
    abstract suspend fun deleteAllCookies()
}
