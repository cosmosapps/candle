package com.cosmos.candelabra.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.cosmos.candelabra.data.local.dao.CookieDao
import com.cosmos.candelabra.data.local.dao.QuoteDao
import com.cosmos.candelabra.data.model.db.Cookie
import com.cosmos.candelabra.data.model.db.Quote

@Database(
    entities = [Quote::class, Cookie::class],
    version = 2,
    exportSchema = true
)
abstract class StockDatabase : RoomDatabase() {

    abstract fun quoteDao(): QuoteDao

    abstract fun cookieDao(): CookieDao

    companion object {
        suspend fun prepopulateQuoteTable(quoteDao: QuoteDao) {
            quoteDao.insert(
                Quote("AAPL", "Apple Inc.", "USD", 0.0, 0.0, 0.0, 0.0),
                Quote("GOOG", "Alphabet Inc.", "USD", 0.0, 0.0, 0.0, 0.0),
                Quote("TSLA", "Tesla, Inc.", "USD", 0.0, 0.0, 0.0, 0.0),
                Quote("MSFT", "Microsoft Corporation", "USD", 0.0, 0.0, 0.0, 0.0)
            )
        }

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    CREATE TABLE IF NOT EXISTS `cookie` (
                        `name` TEXT NOT NULL, 
                        `value` TEXT NOT NULL, 
                        `expires_at` INTEGER NOT NULL, 
                        `domain` TEXT NOT NULL, 
                        `path` TEXT NOT NULL, 
                        `secure` INTEGER NOT NULL, 
                        `http_only` INTEGER NOT NULL, 
                        `persistent` INTEGER NOT NULL, 
                        `host_only` INTEGER NOT NULL, 
                        PRIMARY KEY(`name`)
                    )
                    """.trimIndent())
            }
        }
    }
}
