package com.cosmos.candelabra

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.cosmos.candelabra.data.repository.PreferencesRepository
import com.cosmos.candelabra.data.repository.StockRepository
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class CandelabraApplication : Application() {

    @Inject
    lateinit var preferencesRepository: PreferencesRepository

    @Inject
    lateinit var stockRepository: StockRepository

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        runBlocking { stockRepository.getAllQuotes().first() }

        runBlocking {
            val nightMode = preferencesRepository.getNightMode().first()
            AppCompatDelegate.setDefaultNightMode(nightMode)
        }
    }
}
